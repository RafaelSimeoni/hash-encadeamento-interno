#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define TABLE_SIZE 1000

typedef struct no {
  char* chave;
  int valor;
  struct no* proximo;
} No;

No* tabela[TABLE_SIZE];

unsigned int funcao_hash(char* chave) {
  unsigned int valor = 0;
  for (int i = 0; i < strlen(chave); i++) {
    valor = valor * 31 + chave[i];
  }
  return valor % TABLE_SIZE;
}

void inserir(char* chave, int valor) {
  unsigned int indice = funcao_hash(chave);
  No* no = (No*) malloc(sizeof(No));
  no->chave = chave;
  no->valor = valor;
  no->proximo = tabela[indice];
  tabela[indice] = no;
}

int buscar(char* chave) {
  unsigned int indice = funcao_hash(chave);
  No* no = tabela[indice];
  while (no != NULL) {
    if (strcmp(no->chave, chave) == 0) {
      return no->valor;
    }
    no = no->proximo;
  }
  return -1;
}

void remover(char* chave) {
  unsigned int indice = funcao_hash(chave);
  No* anterior = NULL;
  No* no = tabela[indice];
  while (no != NULL) {
    if (strcmp(no->chave, chave) == 0) {
      if (anterior == NULL) {
        tabela[indice] = no->proximo;
      } else {
        anterior->proximo = no->proximo;
      }
      free(no);
      return;
    }
    anterior = no;
    no = no->proximo;
  }
}

int main() {
  inserir("chave1", 1);
  inserir("chave2", 2);
  printf("%d\n", buscar("chave1"));
  printf("%d\n", buscar("chave2"));
  remover("chave1");
  printf("%d\n", buscar("chave1"));
  return 0;
}
